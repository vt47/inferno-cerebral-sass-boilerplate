import style from './button.scss'

const Button = ({ onClick, title, color }) => (
  <button onClick={onClick} className={style[`btn--${color}`]}>
    {title || 'Title'}
  </button>
)

export default Button
