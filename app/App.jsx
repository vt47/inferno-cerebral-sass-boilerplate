import Banner from './components/banner'
import Button from './components/base/Button'
import initial from './manage/state'

console.log(initial)

const App = () => (
  <div>
    <Banner />
    <div style={{ textAlign: 'center', marginTop: '3rem' }}>
      <Button title="Hi!" color="red" />
      <Button title="Hi!" color="green" />
    </div>
  </div>
)

export default App
