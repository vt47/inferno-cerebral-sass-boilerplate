import { Router, Route, Redirect } from 'inferno-router'
import createBrowserHistory from 'history/createBrowserHistory'
import App from './App'

const browserHistory = createBrowserHistory()

const routes = (
  <Router history={browserHistory}>
    <Redirect from="/" to="/app" />
    <Route path="/app" component={App} />
  </Router>
)

Inferno.render(routes, document.getElementById('app'))
