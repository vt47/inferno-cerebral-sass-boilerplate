exports.npm = {
  globals: {
    Inferno: 'inferno',
    Component: 'inferno-component'
  }
}

exports.files = {
  javascripts: {
    entryPoints: {
      'app/initialize.js': 'app.js'
    },
    joinTo: 'app.js'
  },
  stylesheets: {
    joinTo: 'app.css'
  }
}

exports.modules = {
  autoRequire: {
    'app.js': ['initialize']
  }
}

exports.plugins = {
  sass: {
    mode: 'native',
    sourceMapEmbed: true,
    precision: 8,
    modules: {
      generateScopedName: '[name]__[local]___[hash:base64:5]',
      ignore: ['app/styles/*.scss']
    },
    options: {
      includePaths: ['node_modules/ress']
    }
  },
  uglify: {
    mangle: true,
    compress: {
      global_defs: {
        ENV: 'production'
      }
    }
  }
}
